//
//  SSTabBarController.m
//  SSTabBarController
//
//  Created by Susim on 6/27/14.
//  Copyright (c) 2014 Susim. All rights reserved.
//

#import "SSTabBarController.h"
#import "AppDelegate.h"


@interface SSTabBarController ()
@property (nonatomic,weak)  UIView *customTabContainer;
- (IBAction)selectedTab:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *imgRes;
@property (weak, nonatomic) IBOutlet UIImageView *imgMap;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;

@end

@implementation SSTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self hideExistingTabBar];
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SSTabBarController" owner:self options:nil];
    self.customTabContainer = [nibObjects objectAtIndex:0];
    
    //add bar
    UILabel *lblGray = [[UILabel alloc]initWithFrame:CGRectMake(0, (self.view.frame.size.height-self.customTabContainer.frame.size.height) - 1 ,self.view.frame.size.width, 1)];
    lblGray.backgroundColor = [UIColor lightGrayColor];
    
     [self.view addSubview:lblGray];
    
    self.customTabContainer.frame = CGRectMake(0, self.view.frame.size.height-self.customTabContainer.frame.size.height, self.view.frame.size.width, self.customTabContainer.frame.size.height);
    [self.view addSubview:self.customTabContainer];
   [self selectedTab:self.customTabContainer.subviews[1]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (void)hideExistingTabBar {
	for(UIView *view in self.view.subviews){
		if([view isKindOfClass:[UITabBar class]]){
			view.hidden = YES;
			break;
        }
    }
}
- (void) processCompleted:(NSInteger)btnTag{
    [self setSelectedIndex:btnTag];
}
- (IBAction)selectedTab:(id)sender {
    
    
    UIButton *tabBtn = (UIButton *)sender;
    if(tabBtn.tag==0){
        self.imgRes.highlighted = YES;
         self.imgMap.highlighted = NO;
         self.imgProfile.highlighted = NO;
    }else if (tabBtn.tag==1){
        self.imgMap.highlighted = YES;
        self.imgRes.highlighted = NO;
        self.imgProfile.highlighted = NO;

    }else if (tabBtn.tag==2){
        self.imgProfile.highlighted = YES;
        self.imgRes.highlighted = NO;
        self.imgMap.highlighted = NO;

    }
    [self setSelectedIndex:tabBtn.tag];
    
}
@end
