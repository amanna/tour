//
//  SSTabBarController.h
//  SSTabBarController
//
//  Created by Susim on 6/27/14.
//  Copyright (c) 2014 Susim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DrawConfirmVC.h"
@protocol tabbardelegate <NSObject>
//@required
//- (void) processCompleted:(NSInteger)btnTag;
//@end
@end
@interface SSTabBarController : UITabBarController<drawdelegate,tabbardelegate>{
    id <tabbardelegate> _delegate;
}
- (void)hideExistingTabBar;
- (void) processCompleted:(NSInteger)btnTag;
@property (nonatomic,strong) id delegate;
@end
